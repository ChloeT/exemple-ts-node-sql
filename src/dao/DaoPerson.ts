import {Connection, createConnection} from "mysql";

import { promisify } from "util";
import { Person } from "../entity/Person";



export class DaoPerson {
    private query;
    constructor() {
        //Création de la connexion base de données à externaliser (pour pouvoir l'utiliser dans plusieurs DAO)
        const connection = createConnection({
            host:'localhost',
            database:'node',
            user:'simplon',
            password: '1234'
        });
        this.query = promisify(connection.query).bind(connection);
    }

    /**
     * On utilise les async await pour gérer l'asynchrone de manière plus "jolie"/lisible
     * On fait la requête, puis on itère sur les résultats pour les transformer en instance
     * de l'entité désirée
     */
    async findAllPerson():Promise<Person[]> {
        let result = await this.query("SELECT * FROM person");
        //On peut faire la conversion en entité avec un map
        return result.map(row => new Person(row['name'], row['age'], row['id']));


        //Ou bien avec une boucle
        /*
        let persons = [];
        for(const row  of result) {
            persons.push(new Person(row['name'], row['age'], row['id']));
        }
        return persons;
        */
    }

    async addPerson(person:Person):Promise<number> {
        let result = await this.query('INSERT INTO person (name,age) VALUES (?,?)', [
            person.getName(),
            person.getAge()
        ]);
        let id = result.insertId;
        person.setId(id);
        return id;
    }
}