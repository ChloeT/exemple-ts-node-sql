import { DaoPerson } from "./dao/DaoPerson";
import { Person } from "./entity/Person";

const dao = new DaoPerson();

async function addUsageExample() {
    let person = new Person('Paul', 43);
    await dao.addPerson(person);
    console.log(person);
}

async function findUsageExample() {
    let persons = await dao.findAllPerson();
    console.log(persons);
}


findUsageExample();
addUsageExample();